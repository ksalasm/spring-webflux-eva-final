package com.ksm.security;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author ksalasm on 20/09/22
 * @project IntelliJ IDEA
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
//Clase S2
public class AuthRequest {

    private String username;
    private String password;
}
