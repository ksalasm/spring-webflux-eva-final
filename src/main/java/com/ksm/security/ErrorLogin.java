package com.ksm.security;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 * @author ksalasm on 20/09/22
 * @project IntelliJ IDEA
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ErrorLogin {

    private String message;
    private Date timestamp;
}
