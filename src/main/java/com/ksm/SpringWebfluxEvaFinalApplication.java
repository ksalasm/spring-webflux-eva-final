package com.ksm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWebfluxEvaFinalApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringWebfluxEvaFinalApplication.class, args);
    }

}
