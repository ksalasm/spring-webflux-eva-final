package com.ksm.service;

import com.ksm.model.Menu;
import reactor.core.publisher.Flux;

/**
 * @author ksalasm on 21/09/22
 * @project IntelliJ IDEA
 */
public interface IMenuService extends ICRUD<Menu, String>{

    Flux<Menu> getMenus(String[] roles);
}
