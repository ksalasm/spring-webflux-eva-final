package com.ksm.service;

import com.ksm.model.User;
import reactor.core.publisher.Mono;

/**
 * @author ksalasm on 20/09/22
 * @project IntelliJ IDEA
 */
public interface IUserService extends ICRUD<User, String> {

    Mono<User> saveHash(User user);
    Mono<com.ksm.security.User> searchByUser(String user);
}
