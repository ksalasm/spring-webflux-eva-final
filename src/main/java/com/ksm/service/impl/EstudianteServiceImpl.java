package com.ksm.service.impl;

import com.ksm.model.Estudiante;
import com.ksm.repository.IEstudianteRepo;
import com.ksm.repository.IGenericRepo;
import com.ksm.service.IEstudianteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

/**
 * @author ksalasm on 2/10/22
 * @project IntelliJ IDEA
 */

@Service
public class EstudianteServiceImpl extends ICRUDImpl<Estudiante, String> implements IEstudianteService {

    @Autowired
    private IEstudianteRepo repo;

    @Override
    protected IGenericRepo<Estudiante, String> getRepo() {
        return repo;
    }


    @Override
    public Flux<Estudiante> findByOrderByEdadAsc() {
        return repo.findByOrderByEdadAsc();
    }

    @Override
    public Flux<Estudiante> findByOrderByEdadDesc() {
        return repo.findByOrderByEdadDesc();
    }
}
