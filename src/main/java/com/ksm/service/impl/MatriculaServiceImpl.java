package com.ksm.service.impl;

import com.ksm.model.Matricula;
import com.ksm.repository.IGenericRepo;
import com.ksm.repository.IMatriculaRepo;
import com.ksm.service.IMatriculaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author ksalasm on 2/10/22
 * @project IntelliJ IDEA
 */

@Service
public class MatriculaServiceImpl extends ICRUDImpl<Matricula, String> implements IMatriculaService {

    @Autowired
    private IMatriculaRepo repo;

    @Override
    protected IGenericRepo<Matricula, String> getRepo() {
        return repo;
    }
}
