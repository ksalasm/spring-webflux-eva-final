package com.ksm.service.impl;

import com.ksm.model.Menu;
import com.ksm.repository.IGenericRepo;
import com.ksm.repository.IMenuRepo;
import com.ksm.service.IMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

/**
 * @author ksalasm on 21/09/22
 * @project IntelliJ IDEA
 */
@Service
public class MenuServicesImpl extends ICRUDImpl<Menu, String> implements IMenuService {

    @Autowired
    private IMenuRepo repo;

    @Override
    protected IGenericRepo<Menu, String> getRepo() {
        return repo;
    }


    @Override
    public Flux<Menu> getMenus(String[] roles) {
        return repo.getMenus(roles);
    }
}
