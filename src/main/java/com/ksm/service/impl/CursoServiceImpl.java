package com.ksm.service.impl;

import com.ksm.model.Curso;
import com.ksm.repository.ICursoRepo;
import com.ksm.repository.IGenericRepo;
import com.ksm.service.ICursoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author ksalasm on 2/10/22
 * @project IntelliJ IDEA
 */

@Service
public class CursoServiceImpl extends ICRUDImpl<Curso, String> implements ICursoService {

    @Autowired
    private ICursoRepo repo;

    @Override
    protected IGenericRepo<Curso, String> getRepo() {
        return repo;
    }
}
