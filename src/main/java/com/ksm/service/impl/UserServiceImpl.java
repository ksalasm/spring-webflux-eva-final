package com.ksm.service.impl;

import com.ksm.model.User;
import com.ksm.repository.IGenericRepo;
import com.ksm.repository.IRoleRepo;
import com.ksm.repository.IUserRepo;
import com.ksm.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ksalasm on 20/09/22
 * @project IntelliJ IDEA
 */

@Service
public class UserServiceImpl extends ICRUDImpl<User, String> implements IUserService {

    @Autowired
    private IUserRepo repo;

    @Autowired
    private IRoleRepo rolRepo;

    @Autowired
    private BCryptPasswordEncoder bcrypt;

    @Override
    protected IGenericRepo<User, String> getRepo() {
        return repo;
    }

    @Override
    public Mono<com.ksm.security.User> searchByUser(String username) {
        Mono<com.ksm.model.User> monoUser = repo.findOneByUsername(username);

        List<String> roles = new ArrayList<>();

        return monoUser.flatMap(u -> {
                    return Flux.fromIterable(u.getRoles())
                            .flatMap(rol -> {
                                return rolRepo.findById(rol.getId())
                                        .map(r -> {
                                            roles.add(r.getName());
                                            return r;
                                        });
                            }).collectList().flatMap(list -> {
                                u.setRoles(list);
                                return Mono.just(u);
                            });
                })
                .flatMap(u -> {
                    return Mono.just(new com.ksm.security.User(u.getUsername(), u.getPassword(), u.getStatus(), roles));
                });
    }

    @Override
    public Mono<com.ksm.model.User> saveHash(com.ksm.model.User user) {
        user.setPassword(bcrypt.encode(user.getPassword()));
        return repo.save(user);
    }
}
