package com.ksm.service.impl;

import com.ksm.repository.IGenericRepo;
import com.ksm.service.ICRUD;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author ksalasm on 2/10/22
 * @project IntelliJ IDEA
 */
public abstract class ICRUDImpl<T, ID> implements ICRUD<T, ID> {

    protected abstract IGenericRepo<T, ID> getRepo();

    @Override
    public Flux<T> findAll() {
        return getRepo().findAll();
    }

    @Override
    public Mono<T> findById(ID id) {
        return getRepo().findById(id);
    }

    @Override
    public Mono<T> save(T t) {
        return getRepo().save(t);
    }

    @Override
    public Mono<T> update(T t) {
        return getRepo().save(t);
    }

    @Override
    public Mono<Void> deleteById(ID id) {
        return getRepo().deleteById(id);
    }
}
