package com.ksm.service;

import com.ksm.model.Matricula;

/**
 * @author ksalasm on 2/10/22
 * @project IntelliJ IDEA
 */
public interface IMatriculaService extends ICRUD<Matricula, String> {
}
