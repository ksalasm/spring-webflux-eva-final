package com.ksm.service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author ksalasm on 2/10/22
 * @project IntelliJ IDEA
 */
public interface ICRUD<T, ID> {

    Flux<T> findAll();
    Mono<T> findById(ID id);
    Mono<T> save(T t);
    Mono<T> update(T t);
    Mono<Void> deleteById(ID id);
}
