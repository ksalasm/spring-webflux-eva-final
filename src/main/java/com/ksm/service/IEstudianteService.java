package com.ksm.service;

import com.ksm.model.Estudiante;
import reactor.core.publisher.Flux;

/**
 * @author ksalasm on 2/10/22
 * @project IntelliJ IDEA
 */
public interface IEstudianteService extends ICRUD<Estudiante, String> {

    Flux<Estudiante> findByOrderByEdadAsc();
    Flux<Estudiante> findByOrderByEdadDesc();
}
