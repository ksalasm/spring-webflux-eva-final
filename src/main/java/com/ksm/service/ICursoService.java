package com.ksm.service;

import com.ksm.model.Curso;

/**
 * @author ksalasm on 2/10/22
 * @project IntelliJ IDEA
 */
public interface ICursoService extends ICRUD<Curso, String> {
}
