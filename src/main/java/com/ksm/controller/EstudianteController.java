package com.ksm.controller;

import com.ksm.model.Estudiante;
import com.ksm.service.IEstudianteService;
import com.ksm.validator.RequestValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.Comparator;

/**direction
 * @author ksalasm on 2/10/22direction
 * @project IntelliJ IDEA
 */

@RestController
@RequestMapping("/estudiantes")
public class EstudianteController {

    @Autowired
    private IEstudianteService service;

    @Autowired
    private RequestValidator requestValidator;

    @GetMapping
    public Mono<ResponseEntity<Flux<Estudiante>>> findAll() {
        Flux<Estudiante> estudiantes = service.findAll();
        return Mono.just(ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(estudiantes));
    }

    @GetMapping("/{id}")
    public Mono<ResponseEntity<Estudiante>> findById(@PathVariable("id") String id) {
        return service.findById(id)
                .map(e -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(e))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PostMapping
    public Mono<ResponseEntity<Estudiante>> save(@RequestBody Estudiante estudiante) {
        return Mono.just(estudiante)
                .flatMap(requestValidator::validate)
                .flatMap(service::save)
                .map(e -> ResponseEntity.status(HttpStatus.CREATED)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(e));
    }

    @PutMapping("/{id}")
    public Mono<ResponseEntity<Estudiante>> update(@Valid @PathVariable("id") String id, @RequestBody Estudiante estudiante) {
        Mono<Estudiante> estudianteDB = service.findById(id);
        Mono<Estudiante> estudianteBean = Mono.just(estudiante);

        return estudianteDB.zipWith(estudianteBean, (estDB, estBean) -> {
            estDB.setId(id);
            estDB.setNombres(estBean.getNombres());
            estDB.setApellidos(estBean.getApellidos());
            estDB.setEdad(estBean.getEdad());
            estDB.setDni(estBean.getDni());
            return estDB;
        })
                .flatMap(requestValidator::validate)
                .flatMap(service::update)
                .map(e -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(e))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Void>> deleteById(@PathVariable("id") String id) {
        return service.findById(id)
                .flatMap(c -> service.deleteById(c.getId())
                        .thenReturn(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @GetMapping("/order-asc-programatica")
    public Mono<ResponseEntity<Flux<Estudiante>>> findAllOrderAsc() {
        Flux<Estudiante> estudiantes = service.findAll().sort(Comparator.comparing(Estudiante::getEdad));
        return Mono.just(ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(estudiantes));
    }

    @GetMapping("/order-desc-programatica")
    public Mono<ResponseEntity<Flux<Estudiante>>> findAllOrderDesc() {
        Flux<Estudiante> estudiantes = service.findAll().sort(Comparator.comparing(Estudiante::getEdad).reversed());
        return Mono.just(ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(estudiantes));
    }

    @GetMapping("/order-asc-bd")
    public Mono<ResponseEntity<Flux<Estudiante>>> findAllBDAsc() {
        Flux<Estudiante> estudiantes = service.findByOrderByEdadAsc();
        return Mono.just(ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(estudiantes));
    }

    @GetMapping("/order-desc-bd")
    public Mono<ResponseEntity<Flux<Estudiante>>> findAllBDDesc() {
        Flux<Estudiante> estudiantes = service.findByOrderByEdadDesc();
        return Mono.just(ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(estudiantes));
    }

}
