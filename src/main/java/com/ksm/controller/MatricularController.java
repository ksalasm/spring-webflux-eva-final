package com.ksm.controller;

import com.ksm.model.Matricula;
import com.ksm.service.IMatriculaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;

/**
 * @author ksalasm on 2/10/22
 * @project IntelliJ IDEA
 */

@RestController
@RequestMapping("/matriculas")
public class MatricularController {

    @Autowired
    private IMatriculaService service;

    @GetMapping
    public Mono<ResponseEntity<Flux<Matricula>>> findAll() {
        Flux<Matricula> matriculas = service.findAll();

        return Mono.just(ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(matriculas));
    }

    @GetMapping("/{id}")
    public Mono<ResponseEntity<Matricula>> findById(@PathVariable("id") String id) {
        return service.findById(id)
                .map(m -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(m))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PostMapping
    public Mono<ResponseEntity<Matricula>> save(@RequestBody Matricula matricula) {
        matricula.setFecha(LocalDateTime.now());
        matricula.setEstado(Boolean.TRUE);

        return service.save(matricula)
                .map(m -> ResponseEntity.status(HttpStatus.CREATED)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(m));
    }

    @PutMapping("/{id}")
    public Mono<ResponseEntity<Matricula>> update(@PathVariable("id") String id, @RequestBody Matricula matricula) {
        Mono<Matricula> matriculaBD = service.findById(id);
        Mono<Matricula> matriculaBean = Mono.just(matricula);

        return matriculaBD.zipWith(matriculaBean, (matDB, matBean) -> {
                    matDB.setId(id);
                    matDB.setEstudiante(matBean.getEstudiante());
                    matDB.setFecha(matBean.getFecha());
                    matDB.setEstado(matBean.getEstado());
                    matDB.setCursos(matBean.getCursos());
                    return  matDB;
                })
                .flatMap(service::update)
                .map(m -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(m))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Void>> deleteById(@PathVariable("id") String id) {
        return service.findById(id)
                .flatMap(m -> service.deleteById(m.getId())
                        .thenReturn(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }
}
