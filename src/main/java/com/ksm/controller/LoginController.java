package com.ksm.controller;

import com.ksm.model.User;
import com.ksm.security.AuthRequest;
import com.ksm.security.AuthResponse;
import com.ksm.security.ErrorLogin;
import com.ksm.security.JWTUtil;
import com.ksm.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ksalasm on 20/09/22
 * @project IntelliJ IDEAreturn
 */

//Clase S8
@RestController
public class LoginController {

    @Autowired
    private JWTUtil jwtUtil;

    @Autowired
    private IUserService service;

    @PostMapping("/login")
    public Mono<ResponseEntity<?>> login(@RequestBody AuthRequest ar) {
        return service.searchByUser(ar.getUsername())
                .map((userDetails) -> {
                    if (BCrypt.checkpw(ar.getPassword(), userDetails.getPassword())) {
                        String token = jwtUtil.generateToken(userDetails);
                        Date expiration = jwtUtil.getExpirationDateFromToken(token);

                        return ResponseEntity.ok(new AuthResponse(token, expiration));
                    } else {
                        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new ErrorLogin("Credenciales incorrectas", new Date()));
                    }
                }).defaultIfEmpty(ResponseEntity.status(HttpStatus.UNAUTHORIZED).build());
    }

    @PostMapping("/login-registry")
    public Mono<ResponseEntity<?>> loginRegistry(@RequestBody User user) {
        return service.saveHash(user)
                .map(u -> {
                    Map<String, Object> result = new HashMap<>();
                    result.put("Message", "Se registro usuario con éxito");
                    result.put("User", u);

                    return ResponseEntity.status(HttpStatus.CREATED)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(result);
                });

    }

}
