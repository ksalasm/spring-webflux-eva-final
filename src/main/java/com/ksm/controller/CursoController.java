package com.ksm.controller;

import com.ksm.model.Curso;
import com.ksm.service.ICursoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author ksalasm on 2/10/22
 * @project IntelliJ IDEA
 */

@RestController
@RequestMapping("/cursos")
public class CursoController {

    @Autowired
    private ICursoService service;

    @GetMapping
    public Mono<ResponseEntity<Flux<Curso>>> findAll() {
        Flux<Curso> cursos = service.findAll();

        return Mono.just(ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(cursos));
    }

    @GetMapping("/{id}")
    public Mono<ResponseEntity<Curso>> findById(@PathVariable("id") String id) {
        return service.findById(id)
                .map(e -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(e))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PostMapping
    public Mono<ResponseEntity<Curso>> save(@RequestBody Curso curso) {
        return service.save(curso).
                map(c -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(c));
    }

    @PutMapping("/{id}")
    public Mono<ResponseEntity<Curso>> update(@PathVariable("id") String id, @RequestBody Curso curso) {
        Mono<Curso> cursoBody = Mono.just(curso);
        Mono<Curso> cursoBD = service.findById(id);

        return cursoBD.zipWith(cursoBody, (bd, c) -> {
            bd.setId(id);
            bd.setNombre(c.getNombre());
            bd.setSiglas(c.getSiglas());
            bd.setEstado(c.getEstado());
            return bd;
        }).flatMap(service::update)
                .map(c -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(c))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Void>> deleteById(@PathVariable("id") String id) {
        return service.findById(id)
                .flatMap(c -> service.deleteById(c.getId())
                        .thenReturn(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }



}
