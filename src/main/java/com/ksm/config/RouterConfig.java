package com.ksm.config;

import com.ksm.handler.CursoHandler;
import com.ksm.handler.EstudianteHandler;
import com.ksm.handler.MatriculaHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.*;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

/**
 * @author ksalasm on 12/09/22
 * @project IntelliJ IDEA
 */
@Configuration
public class RouterConfig {

    @Bean
    public RouterFunction<ServerResponse> routesEstudiantes(EstudianteHandler handler) {
        return route(GET("/v2/estudiantes"), handler::findAll)
                .andRoute(GET("/v2/estudiantes/{id}"), handler::findById)
                .andRoute(POST("/v2/estudiantes"), handler::create)
                .andRoute(PUT("/v2/estudiantes/{id}"), handler::update)
                .andRoute(DELETE("/v2/estudiantes/{id}"), handler::deleteById);
    }

    @Bean
    public RouterFunction<ServerResponse> routesCursos(CursoHandler handler) {
        return route(GET("/v2/cursos"), handler::findAll)
                .andRoute(GET("/v2/cursos/{id}"), handler::findById)
                .andRoute(POST("/v2/cursos"), handler::create)
                .andRoute(PUT("/v2/cursos/{id}"), handler::update)
                .andRoute(DELETE("/v2/cursos/{id}"), handler::deleteById);
    }

    @Bean
    public RouterFunction<ServerResponse> routesMatriculas(MatriculaHandler handler) {
        return route(GET("/v2/matriculas"), handler::findAll)
                .andRoute(GET("/v2/matriculas/{id}"), handler::findById)
                .andRoute(POST("/v2/matriculas"), handler::create)
                .andRoute(PUT("/v2/matriculas/{id}"), handler::update)
                .andRoute(DELETE("/v2/matriculas/{id}"), handler::deleteById);
    }
}
