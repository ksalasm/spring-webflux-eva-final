package com.ksm.repository;

import com.ksm.model.Curso;

/**
 * @author ksalasm on 2/10/22
 * @project IntelliJ IDEA
 */
public interface ICursoRepo extends IGenericRepo<Curso, String> {
}
