package com.ksm.repository;

import com.ksm.model.User;
import reactor.core.publisher.Mono;

/**
 * @author ksalasm on 20/09/22
 * @project IntelliJ IDEA
 */
public interface IUserRepo extends IGenericRepo<User, String> {

    //SELECT * FROM USER U WHERE U.USERNAME = ?
    //{username : ?}
    //DerivedQueries
    Mono<User> findOneByUsername(String username);
}
