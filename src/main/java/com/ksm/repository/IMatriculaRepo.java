package com.ksm.repository;

import com.ksm.model.Matricula;

/**
 * @author ksalasm on 2/10/22
 * @project IntelliJ IDEA
 */
public interface IMatriculaRepo extends IGenericRepo<Matricula, String> {
}
