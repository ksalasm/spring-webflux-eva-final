package com.ksm.repository;

import com.ksm.model.Role;

/**
 * @author ksalasm on 20/09/22
 * @project IntelliJ IDEA
 */
public interface IRoleRepo extends IGenericRepo<Role, String> {
}
