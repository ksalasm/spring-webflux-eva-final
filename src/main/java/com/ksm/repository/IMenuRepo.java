package com.ksm.repository;

import com.ksm.model.Menu;
import org.springframework.data.mongodb.repository.Query;
import reactor.core.publisher.Flux;

/**
 * @author ksalasm on 21/09/22
 * @project IntelliJ IDEA
 */
public interface IMenuRepo extends IGenericRepo<Menu, String>{

    @Query("{'roles':  { $in:  ?0 }}")
    Flux<Menu> getMenus(String[] roles);
}
