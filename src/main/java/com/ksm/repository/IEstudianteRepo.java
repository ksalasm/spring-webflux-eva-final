package com.ksm.repository;

import com.ksm.model.Estudiante;
import reactor.core.publisher.Flux;

/**
 * @author ksalasm on 2/10/22
 * @project IntelliJ IDEA
 */
public interface IEstudianteRepo extends IGenericRepo<Estudiante, String> {

    Flux<Estudiante> findByOrderByEdadAsc();
    Flux<Estudiante> findByOrderByEdadDesc();
}
