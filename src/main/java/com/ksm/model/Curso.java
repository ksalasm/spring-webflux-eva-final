package com.ksm.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Size;

/**
 * @author ksalasm on 2/10/22
 * @project IntelliJ IDEA
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "curso")
public class Curso {

    @Id
    private String id;
    @Size(min = 3)
    private String nombre;
    @Size(min = 3)
    private String siglas;
    private Boolean estado;
}
