package com.ksm.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author ksalasm on 2/10/22
 * @project IntelliJ IDEA
 */

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "matricula")
public class Matricula {

    @Id
    private String id;
    private LocalDateTime fecha;
    private Estudiante estudiante;
    private List<Curso> cursos;
    private Boolean estado;
}
