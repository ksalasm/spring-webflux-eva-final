package com.ksm.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Size;

/**
 * @author ksalasm on 2/10/22
 * @project IntelliJ IDEA
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "estudiante")
public class Estudiante {

    @Id
    private String id;
    @Size(min = 3)
    private String nombres;
    @Size(min = 3)
    private String apellidos;
    @Size(min = 8, max = 8, message = "Debe tener 8 caracteres")
    private String dni;
    private Integer edad;

}
