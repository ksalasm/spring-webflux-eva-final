package com.ksm.handler;

import com.ksm.model.Estudiante;
import com.ksm.service.IEstudianteService;
import com.ksm.validator.RequestValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

/**
 * @author ksalasm on 3/10/22
 * @project IntelliJ IDEA
 */

@Component
public class EstudianteHandler {

    @Autowired
    private IEstudianteService service;

    @Autowired
    private RequestValidator requestValidator;

    public Mono<ServerResponse> findAll(ServerRequest request) {
        return ServerResponse
                .ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(service.findAll(), Estudiante.class);
    }

    public Mono<ServerResponse> findById(ServerRequest request) {
        String id = request.pathVariable("id");

        return service.findById(id)
                .flatMap(e -> ServerResponse
                        .ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(BodyInserters.fromValue(e))
                )
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> create(ServerRequest request) {
        Mono<Estudiante> estudianteBean = request.bodyToMono(Estudiante.class);

        return  estudianteBean
                .flatMap(requestValidator::validate)
                .flatMap(service::save)
                .flatMap(e -> ServerResponse
                        .status(HttpStatus.CREATED)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(BodyInserters.fromValue(e)));
    }

    public Mono<ServerResponse> update(ServerRequest request) {
        String id = request.pathVariable("id");

        Mono<Estudiante> estudianteBean = request.bodyToMono(Estudiante.class);
        Mono<Estudiante> estudianteBD = service.findById(id);

        return estudianteBD.zipWith(estudianteBean, (estBD, estBean) -> {
            estBD.setId(id);
            estBD.setNombres(estBean.getNombres());
            estBD.setApellidos(estBean.getApellidos());
            estBD.setDni(estBean.getDni());
            estBD.setEdad(estBean.getEdad());
            return estBD;
        })
                .flatMap(requestValidator::validate)
                .flatMap(service::update)
                .flatMap(e -> ServerResponse
                        .ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(BodyInserters.fromValue(e))
                )
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> deleteById(ServerRequest request) {
        String id = request.pathVariable("id");

        return service.findById(id)
                .flatMap(e -> service.deleteById(e.getId())
                        .then(ServerResponse.noContent().build())
                )
                .switchIfEmpty(ServerResponse.notFound().build());
    }

}
