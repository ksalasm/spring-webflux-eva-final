package com.ksm.handler;

import com.ksm.model.Curso;
import com.ksm.model.Estudiante;
import com.ksm.model.Matricula;
import com.ksm.service.ICursoService;
import com.ksm.service.IEstudianteService;
import com.ksm.service.IMatriculaService;
import com.ksm.validator.RequestValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Optional;
import java.util.function.Function;

/**
 * @author ksalasm on 3/10/22
 * @project IntelliJ IDEA
 */

@Component
public class MatriculaHandler {

    @Autowired
    private IMatriculaService service;

    @Autowired
    private IEstudianteService estudianteService;

    @Autowired
    private ICursoService cursoService;

    @Autowired
    private RequestValidator requestValidator;

    public Mono<ServerResponse> findAll(ServerRequest request) {

        Flux<Matricula> result = service.findAll()
                .flatMap(m -> Mono.just(m)
                        .zipWith(estudianteService.findById(m.getEstudiante().getId()), (matricula, estudiante) ->{
                            matricula.setEstudiante(estudiante);
                            return matricula;
                        })
                )
                .flatMap(m -> {
                    return Flux.fromIterable(m.getCursos())
                            .flatMap(c -> {
                                return cursoService.findById(c.getId());
                            }).collectList().flatMap(list -> {
                                m.setCursos(list);
                                return Mono.just(m);
                            });
                });


        return ServerResponse
                .ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(result, Matricula.class);
    }

    public Mono<ServerResponse> findById(ServerRequest request) {
        String id = request.pathVariable("id");

        return service.findById(id)
                .flatMap(e -> ServerResponse
                        .ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(BodyInserters.fromValue(e))
                )
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> create(ServerRequest request) {
        Mono<Matricula> matriculaBean = request.bodyToMono(Matricula.class);

        return  matriculaBean
                .flatMap(requestValidator::validate)
                .flatMap(service::save)
                .flatMap(e -> ServerResponse
                        .status(HttpStatus.CREATED)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(BodyInserters.fromValue(e)));
    }

    public Mono<ServerResponse> update(ServerRequest request) {
        String id = request.pathVariable("id");

        Mono<Matricula> matriculaBean = request.bodyToMono(Matricula.class);
        Mono<Matricula> matriculaBD = service.findById(id);

        return matriculaBD.zipWith(matriculaBean, (bd, bean) -> {
                    bd.setId(id);
                    bd.setFecha(bean.getFecha());
                    bd.setEstado(bean.getEstado());
                    bd.setCursos(bean.getCursos());
                    bd.setEstudiante(bean.getEstudiante());
                    return bd;
                })
                .flatMap(requestValidator::validate)
                .flatMap(service::update)
                .flatMap(e -> ServerResponse
                        .ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(BodyInserters.fromValue(e))
                )
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> deleteById(ServerRequest request) {
        String id = request.pathVariable("id");

        return service.findById(id)
                .flatMap(e -> service.deleteById(e.getId())
                        .then(ServerResponse.noContent().build())
                )
                .switchIfEmpty(ServerResponse.notFound().build());
    }
}
