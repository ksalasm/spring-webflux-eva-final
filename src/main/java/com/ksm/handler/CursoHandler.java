package com.ksm.handler;

import com.ksm.model.Curso;
import com.ksm.service.ICursoService;
import com.ksm.validator.RequestValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

/**
 * @author ksalasm on 3/10/22
 * @project IntelliJ IDEA
 */

@Component
public class CursoHandler {

    @Autowired
    private ICursoService service;

    @Autowired
    private RequestValidator requestValidator;

    public Mono<ServerResponse> findAll(ServerRequest request) {
        return ServerResponse
                .ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(service.findAll(), Curso.class);
    }

    public Mono<ServerResponse> findById(ServerRequest request) {
        String id = request.pathVariable("id");

        return service.findById(id)
                .flatMap(e -> ServerResponse
                        .ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(BodyInserters.fromValue(e))
                )
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> create(ServerRequest request) {
        Mono<Curso> cursoBean = request.bodyToMono(Curso.class);

        return  cursoBean
                .flatMap(requestValidator::validate)
                .flatMap(service::save)
                .flatMap(e -> ServerResponse
                        .status(HttpStatus.CREATED)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(BodyInserters.fromValue(e)));
    }

    public Mono<ServerResponse> update(ServerRequest request) {
        String id = request.pathVariable("id");

        Mono<Curso> cursoBean = request.bodyToMono(Curso.class);
        Mono<Curso> cursoBD = service.findById(id);

        return cursoBD.zipWith(cursoBean, (bd, bean) -> {
                    bd.setId(id);
                    bd.setNombre(bean.getNombre());
                    bd.setSiglas(bean.getSiglas());
                    bd.setEstado(bean.getEstado());
                    return bd;
                })
                .flatMap(requestValidator::validate)
                .flatMap(service::update)
                .flatMap(e -> ServerResponse
                        .ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(BodyInserters.fromValue(e))
                )
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> deleteById(ServerRequest request) {
        String id = request.pathVariable("id");

        return service.findById(id)
                .flatMap(e -> service.deleteById(e.getId())
                        .then(ServerResponse.noContent().build())
                )
                .switchIfEmpty(ServerResponse.notFound().build());
    }
}
